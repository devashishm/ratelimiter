# RateLimiter

_**Use case:**_

Rate Limiting Means ability to make sure your API can be used only for X number of times in a particular time period. As an API provider we have decided to rate limit the callers with X Number calls per Hour per each tenant. If the caller has reached the Limit, we have to return an error code stating same for remaining time of that hour, Once the block period is completed user should be able to access the API. Note : For simplicity let’s assume there is only one API exposed to the tenant and rate Limit is calculated at each clock hour. Tenant : This is a third party company registered for the API calls


_**How to run the application:**_


Suppose a url http://localhost:8080/limit/101 is sent via POSTMAN and a GET request is made then

In this url 101 is the id assumed for any tenant.

In Application.properties file the **rate limit** and **time gap** are mentioned so that they may be changed without impacting the application.

e.g. By default rate limit = 100 , time gap = 3600000 ms (1 hr).

It means tenant 101 can make 100 requests in the 1st hour.
If it makes more than 100 requests then Error Code 429 is sent for TOO MANY REQUESTS.
It it makes within 100 requests then STATUS=OK is sent.

Once it completes one hour then it can again make 100 requests in the next hour.
